package pageobject;

import org.openqa.selenium.WebDriver;

public class BasePageObject
{
    protected static WebDriver driver;

    BasePageObject( WebDriver driver )
    {
        this.driver = driver;
    }
}
