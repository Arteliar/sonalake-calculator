package pageobject;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import config.CalcButton;
import config.CommandCreator;
import config.DataSet;

public class CalculatorPageObject
    extends BasePageObject
{
    private static final By HISTORY_BTN = By.xpath( "//button[@class='btn dropdown-toggle pull-right']" );

    private static final By HISTORY = By.xpath( "//div[@id='histframe']" );

    private static final By HISTORY_LIST = By.xpath( "//div[@id='histframe']/ul/li/p[@class='l']" );

    public CalculatorPageObject( WebDriver driver )
    {
        super( driver );
    }

    public void calc( String cmd, String expected, boolean radian )
    {
        CommandCreator command = new CommandCreator( cmd );
        setRadian( radian );

        for ( CalcButton btn : command.getSequence() )
        {
            pressButton( btn );
        }

        pressButton( CalcButton.RESULT );

        checkResult( expected, cmd );
        pressButton( CalcButton.CLEAR );
    }

    public void checkHistory( List<DataSet> commands )
    {
        WebElement historyPanel = driver.findElement( HISTORY );

        if ( !historyPanel.isDisplayed() )
        {
            driver.findElement( HISTORY_BTN ).click();
        }

        List<WebElement> historyList = driver.findElements( HISTORY_LIST );
        assertEquals( historyList.size(), commands.size() );

        int i = commands.size();
        for ( WebElement el : historyList )
        {
            i--;
            assertEquals( el.getText(), commands.get( i ).getCommand() );
        }
    }

    private void setRadian( boolean radian )
    {
        if ( radian )
        {
            pressButton( CalcButton.RADIAN );
        }
        else
        {
            pressButton( CalcButton.DEGREE );
        }
    }

    private void pressButton( CalcButton btn )
    {
        By btnElement = By.id( buttonId( btn ) );
        driver.findElement( btnElement ).click();
    }

    private void checkResult( String expected, String cmd )
    {
        WebElement el = driver.findElement( By.id( "input" ) );

        String result = el.getAttribute( "value" );
        while ( result.equals( cmd ) )
        {
            result = el.getAttribute( "value" );
        }

        assertEquals( result, expected );
    }

    private String buttonId( CalcButton btn )
    {
        return btn.btnId;
    }
}
