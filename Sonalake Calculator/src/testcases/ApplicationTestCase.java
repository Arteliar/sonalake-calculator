package testcases;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import config.DataSet;
import config.Setup;
import pageobject.CalculatorPageObject;

public class ApplicationTestCase
    extends Setup
{
    private static final List<DataSet> dataSets = new ArrayList<DataSet>();

    @Test
    @Parameters( { "commands", "results", "radians" } )
    public void calc( String commands, String results, String radians )
        throws InterruptedException
    {
        CalculatorPageObject calculator = new CalculatorPageObject( driver );
        setDataSet( commands, results, radians );

        for ( DataSet ds : dataSets )
        {
            calculator.calc( ds.getCommand(), ds.getResult(), ds.getRadian() );
        }

        calculator.checkHistory( dataSets );
    }

    private void setDataSet( String commands, String results, String radians )
    {
        String[] cmd = commands.split( ";" );
        String[] res = results.split( ";" );
        String[] rad = radians.split( ";" );

        for ( int i = 0; i < cmd.length; i++ )
        {
            dataSets.add( new DataSet( cmd[i], res[i], Boolean.parseBoolean( rad[i] ) ) );
        }
    }
}
