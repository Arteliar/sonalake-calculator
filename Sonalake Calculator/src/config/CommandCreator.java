package config;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;

public class CommandCreator
{
    private List<CalcButton> sequence = new ArrayList<CalcButton>();

    public CommandCreator( String input )
    {
        fillSequence( input.toLowerCase() );
    }

    public List<CalcButton> getSequence()
    {
        return sequence;
    }

    private void fillSequence( String input )
    {
        String tmp;
        int lgh;
        while ( input.length() > 0 )
        {
            lgh = 1;
            tmp = input.substring( 0, lgh );
            if ( tmp.equals( "p" ) )
            {
                lgh = 2;
            }
            else if ( tmp.equals( "c" ) )
            {
                lgh = 4;
            }
            else if ( tmp.equals( "s" ) )
            {
                lgh = 5;
            }

            sequence.add( getEnum( input.substring( 0, lgh ) ) );
            input = input.substring( lgh );
        }
    }

    private CalcButton getEnum( String value )
    {
        switch ( value )
        {
            case "0":
                return CalcButton.ZERO;
            case "1":
                return CalcButton.ONE;
            case "2":
                return CalcButton.TWO;
            case "3":
                return CalcButton.THREE;
            case "4":
                return CalcButton.FOUR;
            case "5":
                return CalcButton.FIVE;
            case "6":
                return CalcButton.SIX;
            case "7":
                return CalcButton.SEVEN;
            case "8":
                return CalcButton.EIGHT;
            case "9":
                return CalcButton.NINE;
            case "+":
                return CalcButton.PLUS;
            case "-":
                return CalcButton.MINUS;
            case "/":
                return CalcButton.DIVIDE;
            case "*":
                return CalcButton.MULTIPLY;
            case "(":
                return CalcButton.L_BRACKET;
            case ")":
                return CalcButton.R_BRACKET;
            case "pi":
                return CalcButton.PI;
            case "cos(":
                return CalcButton.COSINE;
            case "sqrt(":
                return CalcButton.SQUARE_ROOT;
            default:
                Assert.fail( "Wrong value in command" );
                return null;
        }
    }
}
