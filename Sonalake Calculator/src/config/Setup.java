package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class Setup
{
    protected WebDriver driver;

    String browser;

    String address;

    private By cookieWindow = By.id( "cookiesmsg" );

    private By cookieDecline = By.xpath( "//button[contains(.,'DECLINE COOKIES')]" );

    @BeforeTest
    protected void setup()
    {
        Properties prop = new Properties();
        InputStream file = null;

        try
        {
            file = new FileInputStream( "configuration/config.properties" );

            prop.load( file );

            browser = prop.getProperty( "browser" ).toLowerCase();
            String driver = prop.getProperty( browser );
            String driverPath = prop.getProperty( browser + "Driver" );

            address = prop.getProperty( "address" );

            System.setProperty( driver, driverPath );

            file.close();
        }
        catch ( IOException ex )
        {
            ex.printStackTrace();
        }
    }

    @BeforeMethod
    public void browserSetup()
    {
        switch ( browser )
        {
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "chrome":
                driver = new ChromeDriver();
                break;
            case "ie":
                driver = new InternetExplorerDriver();
                break;
            default:
                break;
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait( 5, TimeUnit.SECONDS );

        Reporter.log( "Browser session started", true );

        driver.get( address );

        WebElement el = driver.findElement( cookieWindow );
        if ( el.isDisplayed() )
        {
            WebElement btnElement = driver.findElement( cookieDecline );
            btnElement.click();
        }
    }

    @AfterMethod
    public void closeBrowser()
    {
        driver.quit();

        Reporter.log( "Browser closed", true );
    }

}
