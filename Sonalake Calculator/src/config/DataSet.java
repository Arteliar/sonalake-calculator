package config;

public class DataSet
{
    private String command;

    private String result;

    protected boolean radian;

    public DataSet( String command, String result, boolean radian )
    {
        this.command = command;
        this.result = result;
        this.radian = radian;
    }

    public String getCommand()
    {
        return command;
    }

    public String getResult()
    {
        return result;
    }

    public boolean getRadian()
    {
        return radian;
    }
}
