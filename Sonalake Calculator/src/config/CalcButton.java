package config;

public enum CalcButton
{
    ZERO( "Btn0" ),
    ONE( "Btn1" ),
    TWO( "Btn2" ),
    THREE( "Btn3" ),
    FOUR( "Btn4" ),
    FIVE( "Btn5" ),
    SIX( "Btn6" ),
    SEVEN( "Btn7" ),
    EIGHT( "Btn8" ),
    NINE( "Btn9" ),
    PLUS( "BtnPlus" ),
    MINUS( "BtnMinus" ),
    DIVIDE( "BtnDiv" ),
    MULTIPLY( "BtnMult" ),
    RESULT( "BtnCalc" ),
    CLEAR( "BtnClear" ),
    L_BRACKET( "BtnParanL" ),
    R_BRACKET( "BtnParanR" ),
    PI( "BtnPi" ),
    COSINE( "BtnCos" ),
    SQUARE_ROOT( "BtnSqrt" ),
    DEGREE( "trigodeg" ),
    RADIAN( "trigorad" );

    public String btnId;

    private CalcButton( String btnId )
    {
        this.btnId = btnId;
    }
}
